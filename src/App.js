import logo from "./logo.svg";
import "./App.css";
import React, { useEffect, useState } from "react";

import { Fabric } from "./services/fabric";

function App() {
  const [page, setPage] = useState({});
  const [globalElements, setGlobalElements] = useState({});
  const [product, setProduct] = useState({});
  useEffect(() => {
    const fabric = Fabric.init({
      fabricApiUrl: "https://prod01-apigw.custsandbox.fabric.zone",
      account: "60ddb7539e6eb10008ab990b",
      stage: "prod01",
      channel: 12,
      date: "Tue Jul 13 2021 12:34:52 GMT+0500 (Pakistan Standard Time)",
    });
    fabric.xm.getGlobalComponents().then((data) => {
      console.log(data);
      setGlobalElements(data);
    });
    fabric.xm.getPage("/home").then((data) => {
      console.log(data);
      setPage(data);
    });
    fabric.items.getProduct("TEST").then((data) => {
      console.log(data);
      setProduct(data);
    });
  }, []);
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
      <div style={{ textAlign: "start", padding: 50 }}>
        <h2>Page</h2>
        <pre>{JSON.stringify(page, null, 4)}</pre>
        <h2>Global Elements</h2>
        <pre>{JSON.stringify(globalElements, null, 4)}</pre>
        <h2>Product</h2>
        <pre>{JSON.stringify(product, null, 4)}</pre>
      </div>
    </div>
  );
}

export default App;
