export class Fabric {
  static instance;
  config;
  constructor(config) {
    this.config = config;
  }

  static init(config) {
    if (Fabric.instance) {
      return Fabric.instance;
    }
    return new Fabric(config);
  }

  items = {
    getProduct: async (sku) => {
      let product = null;
      try {
        const res = await fetch(
          `${this.config.fabricApiUrl}/api-item/item/product/${sku}`,
          {
            headers: {
              "x-site-context": JSON.stringify({
                account: this.config.account,
                stage: this.config.stage,
                site: this.config.site,
                channel: 12,
                date: new Date().toString(),
              }),
            },
          }
        );
        product = await res.json();
      } catch (e) {}
      return product;
    },
  };
  xm = {
    getPage: async (url) => {
      let page = null;
      try {
        console.log(`${this.config.fabricApiUrl}/api-xpm/page/live?url=${url}`);
        const res = await fetch(
          `${this.config.fabricApiUrl}/api-xpm/page/live?url=${url}`,
          {
            headers: {
              "x-site-context": JSON.stringify({
                account: this.config.account,
                stage: this.config.stage,
                site: this.config.site,
                channel: 12,
                date: new Date().toString(),
              }),
            },
          }
        );

        page = await res.json();
      } catch (e) {}
      return page;
    },
    getGlobalComponents: async () => {
      let page = null;
      try {
        const res = await fetch(
          `${this.config.fabricApiUrl}/api-xpm/global-component/live`,
          {
            headers: {
              "x-site-context": JSON.stringify({
                account: this.config.account,
                stage: this.config.stage,
                channel: 12,
                date: new Date().toString(),
              }),
            },
          }
        );

        page = await res.json();
      } catch (e) {}
      return page;
    },
  };
}

